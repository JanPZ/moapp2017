package com.example.zhoosch.moapp2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class AnimationsActivity01 extends AppCompatActivity {

    Scene mASzene;
    Scene mBSzene;
    ViewGroup mSceneRoot;
    Transition mFadeTransition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animations01);

        mFadeTransition = TransitionInflater.
                from(this).inflateTransition(R.transition.
                fade_transition);

        ToggleButton tb = (ToggleButton) findViewById(R.id.swtAniButton);
        tb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    //mBSzene.enter();
                    TransitionManager.go(mBSzene, mFadeTransition);
                }else{
                    //mASzene.enter();
                    TransitionManager.go(mASzene, mFadeTransition);
                }
            }
        });

        mSceneRoot = (ViewGroup) findViewById(R.id.scene_root);

        mASzene = Scene.getSceneForLayout(mSceneRoot, R.layout.a_szene, this);
        mBSzene = Scene.getSceneForLayout(mSceneRoot, R.layout.b_szene, this);
        mASzene.enter();
    }

}
