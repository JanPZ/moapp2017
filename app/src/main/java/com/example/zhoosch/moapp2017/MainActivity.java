package com.example.zhoosch.moapp2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        Button cmd2L = (Button) findViewById(R.id.cmd_start_2ndlifecycle);
        cmd2L.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SndLevelLivecycle.class);
                startActivity(intent);
            }
        });

        Button cmdAni01 = (Button) findViewById(R.id.cmd_start_animation01);
        cmdAni01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AnimationsActivity01.class);
                startActivity(intent);
            }
        });

        Button cmdDb01 = (Button) findViewById(R.id.cmd_start_Database01try);
        cmdDb01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Database01tryActivity.class);
                startActivity(intent);
            }
        });

        Button cmdSet01 = (Button) findViewById(R.id.cmd_start_settings01);
        cmdSet01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Settings01Activity.class);
                startActivity(intent);
            }
        });

        Button cmdCan01 = (Button) findViewById(R.id.cmd_start_canvas01);
        cmdCan01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Canvas01Activity.class);
                startActivity(intent);
            }
        });

        Button cmdQuiz01 = (Button) findViewById(R.id.cmd_start_quiz01);
        cmdQuiz01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Quiz01Activity.class);
                startActivity(intent);
            }
        });

        Button cmdSensors01 = (Button) findViewById(R.id.cmd_start_sensors01);
        cmdSensors01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Sensors01Activity.class);
                startActivity(intent);
            }
        });

        Button cmdOpenGLES01 = (Button) findViewById(R.id.cmd_start_opengl01);
        cmdOpenGLES01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, OpenGLES20Activity.class);
                startActivity(intent);
            }
        });

        Settings01Activity.showToast(this, "MAIN_onCreate();");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Settings01Activity.showToast(this, "MAIN_onStart();");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Settings01Activity.showToast(this, "MAIN_onResume();");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Settings01Activity.showToast(this, "MAIN_onPause();");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Settings01Activity.showToast(this, "MAIN_onStop();");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Settings01Activity.showToast(this, "MAIN_onDestroy();");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Settings01Activity.showToast(this, "MAIN_onRestart();");
    }
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
