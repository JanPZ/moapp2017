package com.example.zhoosch.moapp2017;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Zhoosch on 26.06.2017.
 */

class MyGLRenderer implements GLSurfaceView.Renderer{
	private Triangle _triangle;
	private Square _square;
	private final float[] _mvpMatrix = new float[16];
	private final float[] _projectionMatrix = new float[16];
	private final float[] _viewMatrix = new float[16];
	private final float[] _rotationMatrix = new float[16];
	public volatile float _angleX = 0.0f;
	public volatile float _angleY = 0.0f;
	@Override
	public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}
	@Override
	public void onDrawFrame(GL10 gl10) {
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		Matrix.setLookAtM(_viewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
		Matrix.multiplyMM(_mvpMatrix, 0, _projectionMatrix, 0, _viewMatrix, 0);
		//_triangle.draw(_mvpMatrix);
		float[] scratch = new float[16];
//		Matrix.setRotateM(_rotationMatrix, 0, _angle, 0, 0, -1.0f);
		Matrix.setRotateM(_rotationMatrix, 0, _angleX, 1.0f, 0, 0);
		Matrix.multiplyMM(scratch, 0, _mvpMatrix, 0, _rotationMatrix, 0);
		Matrix.setRotateM(_rotationMatrix, 0, _angleY, 0, 1.0f, 0);
		Matrix.multiplyMM(scratch, 0, scratch, 0, _rotationMatrix, 0);
		_triangle.draw(scratch);
	}
	@Override
	public void onSurfaceChanged(GL10 gl10, int width, int height) {
		GLES20.glViewport(0, 0, width, height);
		float ratio = (float) width / height;
		Matrix.frustumM(_projectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);

		_triangle = new Triangle();
		_square = new Square();
	}
	public static int loadShader(int type, String shaderCode){
		int shader = GLES20.glCreateShader(type);
		GLES20.glShaderSource(shader, shaderCode);
		GLES20.glCompileShader(shader);

		return shader;
	}
	public float getAngleX(){
		return _angleX;
	}
	public void setAngleX(float inAngle){
		_angleX = inAngle;
	}
	public float getAngleY(){
		return _angleY;
	}
	public void setAngleY(float inAngle){
		_angleY = inAngle;
	}
}
