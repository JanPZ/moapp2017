package com.example.zhoosch.moapp2017;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Zhoosch on 26.06.2017.
 */

public class Square {
	private FloatBuffer vertexBuffer;
	private ShortBuffer drawListBuffer;

	static final int COORDS_PER_VERTEX = 3;
	static float squareCoords[] = {
		-0.5f,  0.5f, 0.0f,		// oben links
		-0.5f, -0.5f, 0.0f,		// unten links
		 0.5f, -0.5f, 0.0f,		// unten rechts
		 0.5f,  0.5f, 0.0f };	// oben rechts

	final short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // Zeichenreihenfolge der vertices

	// TODO: same as Triangle
	public Square(){
		ByteBuffer bb = ByteBuffer.allocateDirect(
				squareCoords.length * 4 );
		bb.order(ByteOrder.nativeOrder());
		vertexBuffer = bb.asFloatBuffer();
		vertexBuffer.put(squareCoords);
		vertexBuffer.position(0);

		// bb für die Drawlist
		ByteBuffer dlb = ByteBuffer.allocateDirect(
				drawOrder.length * 2 );
		dlb.order(ByteOrder.nativeOrder());
		drawListBuffer = dlb.asShortBuffer();
		drawListBuffer.put(drawOrder);
		drawListBuffer.position(0);
	}
}
