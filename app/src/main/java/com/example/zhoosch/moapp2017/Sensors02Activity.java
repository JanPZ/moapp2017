package com.example.zhoosch.moapp2017;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Sensors02Activity extends AppCompatActivity implements SensorEventListener{
	private ProgressBar _pbLightSensor;
	private TextView _tvLightSensor;
	private SensorManager _sensorManager;
	private Sensor _light;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sensors02);
		_pbLightSensor = (ProgressBar) findViewById(R.id.sens02_prog_light);
		_tvLightSensor = (TextView) findViewById(R.id.sens02_txt_light);
		_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		_light		   = _sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		_pbLightSensor.setMax((int)_light.getMaximumRange());
		_pbLightSensor.setIndeterminate(false);

	}


	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		_tvLightSensor.setText(String.format("%012.5f", sensorEvent.values[0]));
		_pbLightSensor.setProgress((int)(sensorEvent.values[0]));
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}

	@Override
	protected void onResume() {
		super.onResume();
		_sensorManager.registerListener(this, _light, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onPause() {
		super.onPause();
		_sensorManager.unregisterListener(this);
	}
}
