package com.example.zhoosch.moapp2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SndLevelLivecycle extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snd_level_livecycle);

        Settings01Activity.showToast(this, "Snd_onCreate();");
    }

    @Override
    protected void onStart() {
        super.onStart();


        Settings01Activity.showToast(this, "Snd_onStart();");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Settings01Activity.showToast(this, "Snd_onResume();");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Settings01Activity.showToast(this, "Snd_onPause();");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Settings01Activity.showToast(this, "Snd_onStop();");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Settings01Activity.showToast(this, "Snd_onDestroy();");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Settings01Activity.showToast(this, "Snd_onRestart();");
    }
}
