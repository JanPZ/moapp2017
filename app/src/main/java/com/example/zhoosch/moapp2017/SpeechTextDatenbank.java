package com.example.zhoosch.moapp2017;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhoosch on 22.05.2017.
 * Helferklasse für die SpeechTextDatenbank
 */

public class SpeechTextDatenbank extends SQLiteOpenHelper {

    private SQLiteDatabase _db;
    private static final String DATENBANK_NAME = "SpeechText.db";
    private static final int DATENBANK_VERSION = 1;

    public SpeechTextDatenbank(Context context){
        super(context, DATENBANK_NAME, null, DATENBANK_VERSION);
    }

    public void setDB(SQLiteDatabase db){
        _db = db;
    }

    public List<String> selectAll_Texts(){
        List<String> results = new ArrayList<String>();
        results.add("Hallo Welt!");
        Cursor cursor = null;
        try {
            cursor = _db.rawQuery(SpeechTextTblI.RQ_SELECT_TEXTS, null);
            for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
                String rowId = cursor.getString(0);
                results.add(rowId);
            }
        }catch(NullPointerException e) {
            Log.e("STD_DB", e.toString());
        }finally {
            if((cursor != null) && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return results ;
    }

    public List<Integer> selectAll_IDs(){
        List<Integer> results = new ArrayList<Integer>();
        results.add(Integer.valueOf(0));
        Cursor cursor = null;
        try {
            cursor = _db.rawQuery(SpeechTextTblI.RQ_SELECT_IDS, null);
            for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
                Integer rowId = cursor.getInt(0);
                results.add(rowId);
            }
        }catch(NullPointerException e) {
            Log.e("STD_DB", e.toString());
        }finally {
            if((cursor != null) && !cursor.isClosed()) {
                cursor.close();
            }
        }
       return results ;
    }

    public String getTextByID(String id){
        String sRet = null;
        Cursor cursor = null;
        try{
            cursor = _db.rawQuery(SpeechTextTblI.RQ_SELECT_ONE_BY_ID, new String[] {id});
            cursor.moveToFirst();
            sRet = cursor.getString(SpeechTextTblI.POS_TEXT4SPEECH);
        }catch(NullPointerException e) {
            Log.e("STD_DB", e.toString());
        }finally {
            if((cursor != null) && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return sRet;
    }

    public Integer getTextByText(String Text){
        Integer iRet = null;
        Cursor cursor = null;
        try{
            cursor = _db.rawQuery(SpeechTextTblI.RQ_SELECT_ONE_BY_TEXT, new String[] {Text});
            cursor.moveToFirst();
            iRet = cursor.getInt(SpeechTextTblI.POS_ID);
        }catch(NullPointerException e) {
            Log.e("STD_DB", e.toString());
        }finally {
            if((cursor != null) && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return iRet;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SpeechTextTblI.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SpeechTextTblI.SQL_DROP);
        onCreate(sqLiteDatabase);
    }

    public interface SpeechTextTblI{
        static String ID = "_id";
        static String TEXT4SPEECH = "text4speech";
        static String FILE4SPEECH = "file4speech";

        static String TABLE_NAME = "speechText";

        static String SQL_CREATE = "CREATE TABLE "+TABLE_NAME+" ( "+
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TEXT4SPEECH + " TEXT NOT NULL, " +
                FILE4SPEECH + " TEXT NOT NULL );";
        static String SQL_UPDATE = "UPDATE "+TABLE_NAME+ " SET "+TEXT4SPEECH+
                " = ?, "+FILE4SPEECH+" = ? WHERE "+ID+" = ?;";
        static String SQL_DROP = "DROP TABLE "+TABLE_NAME;
        static String SQL_FULL_INSERT = "INSERT INTO "+TABLE_NAME+
                " ( "+ TEXT4SPEECH + ", " + FILE4SPEECH + " ) VALUES (?,?);";

        static String RQ_SELECT_ALL = "SELECT * FROM " + TABLE_NAME + ";";
        static String RQ_SELECT_IDS = "SELECT "+ID+" FROM " + TABLE_NAME + ";";
        static String RQ_SELECT_TEXTS = "SELECT "+TEXT4SPEECH+" FROM " + TABLE_NAME + ";";
        static String RQ_SELECT_ONE = "SELECT ? FROM " + TABLE_NAME + ";";
        static String RQ_SELECT_ONE_BY_TEXT = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + TEXT4SPEECH + "=?;";
        static String RQ_SELECT_ONE_BY_ID = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + ID + "=?;";

        static String SQL_DEL_TEXT = "DELETE FROM " + TABLE_NAME + " WHERE " +
                TEXT4SPEECH + "=?";
        static String SQL_DEL_ID =  "DELETE FROM " + TABLE_NAME + " WHERE " +
                ID + "=?";
        static String SQL_DEL_FILE =  "DELETE FROM " + TABLE_NAME + " WHERE " +
                FILE4SPEECH + "=?";

        static final int POS_ID = 0;
        static final int POS_TEXT4SPEECH = 1;
        static final int POS_FILE4SPEECH = 2;
    }
}
