package com.example.zhoosch.moapp2017;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Sensors01Activity extends AppCompatActivity {
	private SensorManager _sensorManager;
	private ListView _sensMainList;
	private List<String> _arrList;
	private List<Sensor> _lsSensors;
	private ArrayAdapter<String> _arrAdapter;
	private Integer iCounter = new Integer(0);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sensors01);
		_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		_lsSensors = _sensorManager.getSensorList(Sensor.TYPE_ALL);
		_sensMainList = (ListView) findViewById(R.id.sensors_main_list);
		_arrList = new ArrayList<>(_lsSensors.size());
		for(Object object : _lsSensors.toArray()){
			_arrList.add(object != null ? object.toString() : null);
		}
		_arrAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, _arrList);
		setListAdapter(_arrAdapter);
		_sensMainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				//addItems(view);
				Intent intent = new Intent(Sensors01Activity.this, Sensors02Activity.class);
				startActivity(intent);
			}
		});
	}

	public void addItems(View view){
		_arrList.add("irgendwas: "+ (iCounter++).toString());
		_arrAdapter.notifyDataSetChanged();
	}

	protected ListView getListView(){
		if(_sensMainList == null){
			_sensMainList = (ListView) findViewById(R.id.sensors_main_list);
		}
		return _sensMainList;
	}

	protected void setListAdapter(ListAdapter listAdapter){
		getListView().setAdapter(_arrAdapter);
	}

	protected ListAdapter getListAdapter(){
		ListAdapter adapter = getListView().getAdapter();
		if(adapter instanceof HeaderViewListAdapter){
			return ((HeaderViewListAdapter)adapter).getWrappedAdapter();
		}else{
			return adapter;
		}
	}
}
