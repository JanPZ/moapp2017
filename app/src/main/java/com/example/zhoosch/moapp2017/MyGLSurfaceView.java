package com.example.zhoosch.moapp2017;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Zhoosch on 26.06.2017.
 */

public class MyGLSurfaceView extends GLSurfaceView{

	private final MyGLRenderer _renderer;
	private final float TOUCH_SCALE_FACTOR = 100.0f / 320;
	private float _PreviousX;
	private float _PreviousY;


	public MyGLSurfaceView(Context context) {
		super(context);

		// OpenGL 2.0
		setEGLContextClientVersion(2);

		_renderer = new MyGLRenderer();

		setRenderer(_renderer);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
//		setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
	}

	@Override
	public boolean onTouchEvent(MotionEvent e){
		float x = e.getX();
		float y = e.getY();

		switch (e.getAction()) {
			case MotionEvent.ACTION_MOVE:
			case MotionEvent.ACTION_HOVER_MOVE:
				float dx = x - _PreviousX;
				float dy = y - _PreviousY;

				if (y > getHeight() / 2) {
					dx = dx * -1;
				}
				if (x < getWidth() / 2) {
					dy = dy * -1;
				}
				_renderer.setAngleX(
						_renderer.getAngleX() *
								(dx * TOUCH_SCALE_FACTOR));
				_renderer.setAngleY(
						_renderer.getAngleY() *
								(dy * TOUCH_SCALE_FACTOR));
				requestRender();
		}
		_PreviousX = x;
		_PreviousY = y;
		return true;
	}
}
