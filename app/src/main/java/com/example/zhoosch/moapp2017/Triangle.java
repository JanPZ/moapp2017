package com.example.zhoosch.moapp2017;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Zhoosch on 26.06.2017.
 */

public class Triangle {

	private int _positionHandel;
	private int _colorHandel;
	private final int vertexCount = triangleCoords.length / COORDS_PER_VERTEX;
	private final int vertexStride = COORDS_PER_VERTEX * 4; // bytes je vertex
	private final int _program;
	private final String vertexShaderCode =
					"uniform mat4 uMVPMatrix;" +
					"attribute vec4 vPosition;" +
					"void main(){" +
					"	gl_Position = uMVPMatrix * vPosition;" +
					"}";
	private final String fragmentShaderCode =
					"precision mediump float;" +
					"uniform vec4 vColor;" +
					"void main(){" +
					"	gl_FragColor = vColor;" +
					"}";
	private int _mvpMatrixHandle;
	private FloatBuffer vertexBuffer;
	static final int COORDS_PER_VERTEX = 3;
	static float triangleCoords[] = {
			 0.0f,  0.622008459f, 0.0f, // Oben
			-0.5f, -0.311004243f, 0.0f, // left
			 0.5f, -0.311004243f, 0.0f  // right
	};

	float color[] = {0.63671875f, 0.76953125f, 0.22265625f, 1.0f};

	public Triangle(){
		ByteBuffer bb = ByteBuffer.allocateDirect(
				triangleCoords.length * 4);
		bb.order(ByteOrder.nativeOrder());
		vertexBuffer = bb.asFloatBuffer();
		vertexBuffer.put(triangleCoords);
		vertexBuffer.position(0);

		int vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCode);
		int fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
				fragmentShaderCode);

		_program = GLES20.glCreateProgram();
		GLES20.glAttachShader(_program, vertexShader);
		GLES20.glAttachShader(_program, fragmentShader);
		GLES20.glLinkProgram(_program);
	}

	public void draw(float[] mvpMatrix){
		GLES20.glUseProgram(_program);
		_positionHandel = GLES20.glGetAttribLocation(_program, "vPosition");
		GLES20.glEnableVertexAttribArray(_positionHandel);
		GLES20.glVertexAttribPointer(_positionHandel, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false,
				vertexStride, vertexBuffer);
		_colorHandel = GLES20.glGetUniformLocation(_program, "vColor");
		GLES20.glUniform4fv(_colorHandel, 1, color, 0);
		_mvpMatrixHandle = GLES20.glGetUniformLocation(_program, "uMVPMatrix");
		GLES20.glUniformMatrix4fv(_mvpMatrixHandle, 1, false, mvpMatrix, 0);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);
		GLES20.glDisableVertexAttribArray(_positionHandel);
	}

}
