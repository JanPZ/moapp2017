package com.example.zhoosch.moapp2017;

import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class OpenGLES20Activity extends AppCompatActivity {

	private GLSurfaceView _GLView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_GLView = new MyGLSurfaceView(this);
		setContentView(_GLView);
	}
}
