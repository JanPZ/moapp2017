package com.example.zhoosch.moapp2017;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class Database01tryActivity extends AppCompatActivity {
    private SpeechTextDatenbank _std;
    private SQLiteDatabase _db;
    private Spinner _spnID, _spnText;
    private Button _cmdDel, _cmdSave, _cmdChange, _cmdSpeak;
    private EditText _txtT4S;
    private TextToSpeech _ttsEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database01try);
        _std = new SpeechTextDatenbank(this);
        _db = _std.getWritableDatabase();
        _std.setDB(_db);
        _ttsEngine = new TextToSpeech(getApplicationContext()
                , new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR){
                    int iRes = _ttsEngine.setLanguage(Locale.GERMAN);
                    if(iRes == TextToSpeech.LANG_MISSING_DATA ||
                            iRes == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Diese Sprache wird nicht unterstützt");
                    }else{
                        _cmdSpeak.setEnabled(true);
                    }
                }
            }
        });

        _spnID = (Spinner) findViewById(R.id.spnDBID);
        _spnText = (Spinner) findViewById(R.id.spnDBText);
        _cmdDel = (Button) findViewById(R.id.cmdDBClean);
        _cmdSave = (Button) findViewById(R.id.cmdDBSave);
        _cmdChange = (Button) findViewById(R.id.cmdDBChange);
        _cmdSpeak = (Button) findViewById(R.id.cmdTTSSpeak);
        _txtT4S = (EditText) findViewById(R.id.txtDBText4Speech);

        _cmdDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _txtT4S.setText("");
                loadSpinnerData();
            }
        });

        _cmdSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_txtT4S.getText().length() < 1) return;

                Log.d("DB","INSERT:"+SpeechTextDatenbank.SpeechTextTblI.SQL_FULL_INSERT);
                _db.execSQL(SpeechTextDatenbank.SpeechTextTblI.SQL_FULL_INSERT
                        , new String[] {_txtT4S.getText().toString(), ""});
                _txtT4S.setText("");
                loadSpinnerData();
            }
        });

        _cmdChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_txtT4S.getText().length() < 1 ||
                        _spnID.getSelectedItem().toString().equals("0")) return;
                _db.execSQL(SpeechTextDatenbank.SpeechTextTblI.SQL_UPDATE
                        , new String[] {_txtT4S.getText().toString(), ""
                                , _spnID.getSelectedItem().toString()});
                loadSpinnerData();
            }
        });
        _cmdSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toSpeak = _txtT4S.getText().toString();
                Integer iRet = _ttsEngine.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID);
                // _ttsEngine.addSpeech(toSpeak, "/dev/null");//"/dev/null");
                Log.d("TTS","onClick() Returnwert der toSpeak():"+iRet.toString());
                //_ttsEngine.addSpeech(toSpeak, getApplication().getPackageName(),R.ra)
            }
        });
        _spnID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(_spnID.getSelectedItem().toString().equals("0")) {
                    this.onNothingSelected(adapterView);
                    return;
                }
                _spnID.setBackgroundColor(Color.GREEN);
                String str = _std.getTextByID(_spnID.getSelectedItem().toString());
                if(str != null) {
                    _txtT4S.setText(str);
                    _spnText.setSelection(_spnID.getSelectedItemPosition());
                }else{
                    Log.e("STD_DB", "Nullreferenz bei call getTextByID");
                }
                _cmdChange.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                _spnID.setBackgroundColor(Color.RED);
                _spnText.setBackgroundColor(Color.RED);
                _cmdChange.setEnabled(false);
            }
        });

        _spnText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(_spnText.getSelectedItem().toString().equals(_spnText.getItemAtPosition(0).toString())) {
                    this.onNothingSelected(adapterView);
                    return;
                }
                _spnText.setBackgroundColor(Color.GREEN);
                Integer iVal = _std.getTextByText(_spnText.getSelectedItem().toString());
                if(iVal != null) {
                   _spnID.setSelection(_spnText.getSelectedItemPosition());
                }else{
                    Log.e("STD_DB", "Nullreferenz bei call getTextByID");
                }
                _cmdChange.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                _spnText.setBackgroundColor(Color.RED);
                _cmdChange.setEnabled(false);
            }
        });
        loadSpinnerData();
    }

    private void loadSpinnerData(){
        int iPos = _spnID.getSelectedItemPosition();


        List<Integer> lstIDs = _std.selectAll_IDs();
        ArrayAdapter<Integer> dataAdapaterID = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, lstIDs);
        dataAdapaterID.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _spnID.setAdapter(dataAdapaterID);

        List<String> lstTexts = _std.selectAll_Texts();
        ArrayAdapter<String> dataAdapaterText = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, lstTexts);
        dataAdapaterText.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _spnText.setAdapter(dataAdapaterText);

        if(iPos > -1){
            _spnID.setSelection(iPos);
            _spnText.setSelection(iPos);
        }
    }

    @Override
    protected void onDestroy() {
        if(_ttsEngine != null){
            //_ttsEngine.stop(); // unnötig
            _ttsEngine.shutdown();
        }
        _db.close();
        _std.close();
        super.onDestroy();
    }
}
